package com.resto.restaurant.controller;

import com.resto.restaurant.model.Dish;
import com.resto.restaurant.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/dish")
@RestController
public class DishController {
    @Autowired
    private DishService dishService;

    @GetMapping("")
    public Iterable<Dish> getDish() {
        return dishService.getDish();
    }

    @GetMapping("/{id}")
    public Optional<Dish> getDish(@PathVariable Long id) {
        return dishService.getDish(id);
    }

    @DeleteMapping("/{id}")
    public void deleteDishByID(@PathVariable Long id) {
        dishService.deleteDishByID(id);
    }

    @PostMapping("")
    private Dish saveDish(@RequestBody Dish dish) {
        return dishService.saveDish(dish);
    }

    @PutMapping("/{id}")
    private Dish updateDish(@PathVariable Long id, @RequestBody Dish dish) {
        return dishService.updateDish(id, dish);
    }
}
