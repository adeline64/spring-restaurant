package com.resto.restaurant.controller;

import com.resto.restaurant.model.Ordered;
import com.resto.restaurant.service.OrderedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/ordered")
@RestController
public class OrderedController {
    @Autowired
    private OrderedService orderedService;

    @GetMapping("")
    public Iterable<Ordered> getOrdered() {
        return orderedService.getOrdered();
    }

    @GetMapping("/{id}")
    public Optional<Ordered> getOrdered(@PathVariable Long id) {
        return orderedService.getOrdered(id);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderedByID(@PathVariable Long id) {
        orderedService.deleteOrderedByID(id);
    }

    @PostMapping("")
    private Ordered saveOrdered(@RequestBody Ordered ordered) {
        return orderedService.saveOrdered(ordered);
    }

    @PutMapping("/{id}")
    private Ordered updateOrdered(@PathVariable Long id, @RequestBody Ordered ordered) {
        return orderedService.updateOrdered(id, ordered);
    }
}
