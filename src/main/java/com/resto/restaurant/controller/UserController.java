package com.resto.restaurant.controller;

import com.resto.restaurant.model.User;
import com.resto.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/users")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public Iterable<User> getUser() {
        return userService.getUser();
    }

    @GetMapping("/{id}")
    public Optional<User> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @DeleteMapping("/{id}")
    public void deleteUserByID(@PathVariable Long id) {
        userService.deleteUserByID(id);
    }

    @PostMapping("")
    private User saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @PutMapping("/{id}")
    private User updateUser(@PathVariable Long id, @RequestBody User user) {
        return userService.updateUser(id, user);
    }
}
