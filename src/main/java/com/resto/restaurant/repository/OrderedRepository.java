package com.resto.restaurant.repository;

import com.resto.restaurant.model.Ordered;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderedRepository extends CrudRepository<Ordered, Long> {
}
