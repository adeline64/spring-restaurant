package com.resto.restaurant.repository;

import com.resto.restaurant.model.Dish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishRepository extends CrudRepository<Dish, Long>  {
}
