package com.resto.restaurant.model;

import jakarta.persistence.*;

@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Ordered.CommandeStatus category;
    public enum category {
        drink,
        entrance,
        main_course,
        dessert;
    }

    public Category() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ordered.CommandeStatus getCategory() {
        return category;
    }

    public void setCategory(Ordered.CommandeStatus category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category=" + category +
                '}';
    }
}
