package com.resto.restaurant.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ordered")
public class Ordered {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "qr_code")
    private String qrCode;
    private CommandeStatus status;
    public enum CommandeStatus {
        wainting_payment,
        wainting_preparation,
        in_preparation,
        completed,
        delivered,
        canceled;
    }
    @OneToOne
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User user;
    @ManyToMany(mappedBy = "ordered")
    private List<Dish> dish = new ArrayList<>();

    public Ordered() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public CommandeStatus getStatus() {
        return status;
    }

    public void setStatus(CommandeStatus status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Dish> getDish() {
        return dish;
    }

    public void setDish(List<Dish> dish) {
        this.dish = dish;
    }

    @Override
    public String toString() {
        return "Ordered{" +
                "id=" + id +
                ", qrCode='" + qrCode + '\'' +
                ", status=" + status +
                ", user=" + user +
                ", dish=" + dish +
                '}';
    }
}
