package com.resto.restaurant.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "dish")
public class Dish {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(name = "price")
    private float price;
    @Column(name = "description")
    private String description;
    @ManyToMany
    @JoinTable(name = "ordered_dish",
            joinColumns = @JoinColumn(name = "dish_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "ordered_id",  referencedColumnName="ID")
    )
    private List<Ordered> ordered = new ArrayList<>();
    @OneToOne
    @JoinColumn(name = "id_category", referencedColumnName = "id")
    private Category category;

    public Dish() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Ordered> getOrdered() {
        return ordered;
    }

    public void setOrdered(List<Ordered> ordered) {
        this.ordered = ordered;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", ordered=" + ordered +
                ", category=" + category +
                '}';
    }
}
