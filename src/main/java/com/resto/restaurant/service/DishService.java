package com.resto.restaurant.service;

import com.resto.restaurant.model.Dish;
import com.resto.restaurant.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DishService {

    @Autowired
    private DishRepository dishRepository;

    public Iterable<Dish> getDish() {
        return dishRepository.findAll();
    }

    public Optional<Dish> getDish(final Long id) {
        return dishRepository.findById(id);
    }

    public void deleteDishByID(final Long id) {
        dishRepository.deleteById(id);
    }

    public Dish saveDish(Dish dish) {
        return dishRepository.save(dish);
    }

    public Dish updateDish(final Long id, Dish dish) {
        var a = dishRepository.findById(id);
        if (a.isPresent()) {
            var courentDish = a.get();
            courentDish.setName(dish.getName());
            courentDish.setPrice(dish.getPrice());
            courentDish.setDescription(dish.getDescription());
            courentDish.setOrdered(dish.getOrdered());
            courentDish.setCategory(dish.getCategory());
            return dishRepository.save(courentDish);
        } else {
            return null;
        }
    }
}
