package com.resto.restaurant.service;

import com.resto.restaurant.model.Ordered;
import com.resto.restaurant.repository.OrderedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class OrderedService {
    @Autowired
    private OrderedRepository orderedRepository;

    public Iterable<Ordered> getOrdered() {
        return orderedRepository.findAll();
    }

    public Optional<Ordered> getOrdered(final Long id) {
        return orderedRepository.findById(id);
    }

    public void deleteOrderedByID(final Long id) {
        orderedRepository.deleteById(id);
    }

    public Ordered saveOrdered(Ordered ordered) {
        return orderedRepository.save(ordered);
    }

    public Ordered updateOrdered(final Long id, Ordered ordered) {
        var a = orderedRepository.findById(id);
        if (a.isPresent()) {
            var courentOrdered = a.get();
            courentOrdered.setDish(ordered.getDish());
            courentOrdered.setUser(ordered.getUser());
            courentOrdered.setQrCode(ordered.getQrCode());
            courentOrdered.setStatus(ordered.getStatus());
            return orderedRepository.save(courentOrdered);
        } else {
            return null;
        }
    }
}
