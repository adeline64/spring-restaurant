package com.resto.restaurant.service;

import com.resto.restaurant.model.User;
import com.resto.restaurant.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getUser() {
        return userRepository.findAll();
    }

    public Optional<User> getUser(final Long id) {
        return userRepository.findById(id);
    }

    public void deleteUserByID(final Long id) {
        userRepository.deleteById(id);
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User updateUser(final Long id, User user) {
        var a = userRepository.findById(id);
        if (a.isPresent()) {
            var courentUser = a.get();
            courentUser.setEmail(user.getEmail());
            courentUser.setPassword(user.getPassword());
            return userRepository.save(courentUser);
        } else {
            return null;
        }
    }
}
