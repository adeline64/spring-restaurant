package com.resto.restaurant.service;

import com.resto.restaurant.model.Category;
import com.resto.restaurant.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public Iterable<Category> getCategory() {
        return categoryRepository.findAll();
    }

    public Optional<Category> getCategory(final Long id) {
        return categoryRepository.findById(id);
    }

    public void deleteCategoryByID(final Long id) {
        categoryRepository.deleteById(id);
    }

    public Category saveCategory(Category category) {
        return categoryRepository.save(category);
    }

    public Category updateCategory(final Long id, Category category) {
        var a = categoryRepository.findById(id);
        if (a.isPresent()) {
            var courentCategory = a.get();
            courentCategory.setCategory(category.getCategory());
            return categoryRepository.save(courentCategory);
        } else {
            return null;
        }
    }

}
